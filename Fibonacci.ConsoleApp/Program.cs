﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fibonacci.ConsoleApp
{
    class Program
    {
        static ulong TempNumber = 0;
        static ulong PrevNumber = 1;
        static ulong NextNumber;
        static int OrderOfNumber;
        static string answer;

        static void Main(string[] args)
        {
            Console.WriteLine("Kaçıncı Fibonacci sayısını görmek istiyorsunuz?");
            OrderOfNumber = Int32.Parse(Console.ReadLine());

            FibonacciCalculate();

            Console.WriteLine( OrderOfNumber + ".Fibonacci Sayısı:" + TempNumber);

            Console.WriteLine("\nİstediğiniz sıradaki Fibonacci sayılarına kadar olan sayıları görmek ister misiniz?(Evet/Hayır)");
            answer = Console.ReadLine().ToUpper();

            if(answer == "EVET")
            {
                FibonacciCalculate();
                Console.Read();
            } 
        }

        static void FibonacciCalculate()
        {
            TempNumber = 0;
            PrevNumber = 1;
            NextNumber = 0;

            for (int i = 0; i < OrderOfNumber; i++)
            {
                NextNumber = PrevNumber + TempNumber;
                PrevNumber = TempNumber;
                TempNumber = NextNumber;

                if(answer == "EVET")
                {
                    if(i < OrderOfNumber - 1)
                    {
                        Console.Write(TempNumber + ",");
                    }
                    else
                    {
                        Console.Write(TempNumber);
                    }
                    
                }

            }
        }
    }
}
